from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Receipt, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def show_receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    user = request.user
    if request.method == "POST":
        form = ReceiptForm(user, request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm(user)
    context = {
        "receipt_form": form
    }
    return render(request, "receipts/create_receipt.html", context)

@login_required
def show_categories(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories
    }
    return render(request, "receipts/categories.html", context)

@login_required
def show_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts":accounts
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            x = form.save(False)
            x.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "category_form": form
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            y = form.save(False)
            y.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "account_form": form
    }
    return render(request, "receipts/create_account.html", context)
